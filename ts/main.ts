/**
 * Created by User on 24.07.2017.
 */

class SynchroniseNavButtonsSlider {

    private controlButtons: string;
    private slider: string;

    constructor(controlButtons: string, slider: string) {
        this.controlButtons = controlButtons;
        this.slider = slider
    }

    listenerControl() {
        let current = this;
        $(this.controlButtons + ' label[data-slide]').on('click', function () {
            let numSlide = $(this).data('slide');
            console.log(numSlide);
            $(current.slider).slick('slickGoTo', numSlide - 1);
        });
    }

    listenerSlider() {
        let current = this;
        $(this.slider).on('afterChange', function (e, slick, currentSlide) {
            console.log(currentSlide);
            $(current.controlButtons + ' label[data-slide=' + (currentSlide + 1) + ']').trigger('click');
        })
    }
}

class Main {

    events() {
        this.clickCollapse();
        this.slidersInit();
        this.likeProduct();
    }

    private clickCollapse() {
        $('.collapse').on('click', function () {
            $(this).toggleClass('collapse-open');
            $('.nav-collapse').toggleClass('nav-collapse-open');
        });
    }

    private likeProduct() {
        $('.like').on('click', function () {
            $(this).toggleClass('is-like');
            $(this).find('[class~="fa"]').toggleClass('fa-heart-o');
            $(this).find('[class~="fa"]').toggleClass('fa-heart');
        });
    }

    private slidersInit() {
        $('.slider-purchase').slick(
            {
                arrows: false,
                infinite: true,
                speed: 300,
                cssEase: 'linear'
            }
        );


        $('.big-slider').slick(
            {
                arrows: false,
                infinite: true,
                speed: 300,
                cssEase: 'linear',
                slidesToShow: 1,
                dots: false,
                asNavFor: '.slider-nav-big-slider'
            }
        );

        $('.slider-nav-big-slider').slick(
            {
                arrows: false,
                infinite: true,
                speed: 300,
                cssEase: 'linear',
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 3,
                focusOnSelect: true,
                dots: false,
                asNavFor: '.big-slider'
            }
        );

        $('.social-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
        });

        let syncNavSlider = new SynchroniseNavButtonsSlider('#control-product-purchase-main', '.big-slider');
        syncNavSlider.listenerControl();
        syncNavSlider.listenerSlider();

        let syncNavSliderPurchase = new SynchroniseNavButtonsSlider('#control-purchase', '.slider-purchase');
        syncNavSliderPurchase.listenerControl();
        syncNavSliderPurchase.listenerSlider();
    }

    private blinkLabel(idInput) {
    $(`#${idInput}`).parent().on('load', () => {
        $(`#${idInput}`).on('focus', () => {
            $(`[for='${idInput}']`).css({"color": "#0089cf"});
        });

        $(`#${idInput}`).on('blur', () => {
            $(`[for='${idInput}']`).css({"color": "#414141"});
        });
    });
}

    static exceptionEvent():void{
        let main: Main = new Main();
        main.blinkLabel("search");
    }

    static main(): void {
        let main: Main = new Main();
        main.events();
    }
}

(function ($) {
    $(function () {
        Main.main();
    });
    Main.exceptionEvent();
})(jQuery);